package mahdziak.cars.saloncars.Repository;

import mahdziak.cars.saloncars.entity.Car;
import mahdziak.cars.saloncars.specification.CarSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    Page<Car> findAll(CarSpecification carSpecification, PageRequest mapToPageRequest);
}

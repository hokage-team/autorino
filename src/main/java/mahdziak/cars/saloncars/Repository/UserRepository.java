package mahdziak.cars.saloncars.Repository;

import mahdziak.cars.saloncars.entity.User;
import mahdziak.cars.saloncars.specification.UserSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    Page<User> findAll(UserSpecification userSpecification, PageRequest mapToPageRequest);
}

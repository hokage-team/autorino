package mahdziak.cars.saloncars.controller;

import mahdziak.cars.saloncars.dto.request.UserSearchRequest;
import mahdziak.cars.saloncars.dto.response.DataResponse;
import mahdziak.cars.saloncars.dto.response.UserResponse;
import mahdziak.cars.saloncars.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class UserController {

    @Autowired
    private UserService userService;


    @PostMapping("/filter")
    public DataResponse<UserResponse> findAllByFilter(@RequestBody UserSearchRequest userSearchRequest) {
        return userService.findAllByFilter(userSearchRequest);
    }



}

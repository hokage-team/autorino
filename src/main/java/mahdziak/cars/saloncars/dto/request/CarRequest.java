package mahdziak.cars.saloncars.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarRequest {

    private String name;

    private Integer year;

    private Integer price;

    private Double volume;

    private Boolean usedCar;

    private Boolean newCar;

    private String condition;



    private Long CountryId;

    private Long ModelId;
}

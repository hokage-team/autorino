package mahdziak.cars.saloncars.dto.request;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserRequest {

    private String lastName;

    private String firstName;

    private String birthData;

    private Integer cardNumber;

    private String email;


}

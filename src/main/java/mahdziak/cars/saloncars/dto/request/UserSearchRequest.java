package mahdziak.cars.saloncars.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserSearchRequest {

    private String lastName;
    private String firstName;
    private String birthData;
    private String email;

    private PaginationRequest paginationRequest;
}

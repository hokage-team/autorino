package mahdziak.cars.saloncars.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mahdziak.cars.saloncars.entity.User;

@Getter
@Setter
@NoArgsConstructor
public class UserResponse {

    private Long id;

    private String lastName;

    private String firstName;

    private String birthData;

    private Integer cardNumber;

    private String email;

    public UserResponse(User user) {
      id = user.getId();
      lastName = user.getLastName();
      firstName = user.getFirstName();
      birthData = user.getBirthData();
<<<<<<< HEAD
      cardNumber = user.getCardNumber();
=======
>>>>>>> 025fe78071af912043d2bc544e8e28eb717cc977
      email = user.getEmail();
    }
}

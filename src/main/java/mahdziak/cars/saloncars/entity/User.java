package mahdziak.cars.saloncars.entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@ToString

@Entity
//@Table(name = "_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String lastName;

    private String firstName;

    private String birthData;

    private String email;

    private Integer cardNumber;

    private String password;




    @OneToMany(mappedBy = "user")
    private List<Car> cars = new ArrayList<>();


    @ManyToOne
    private Country country;






}

package mahdziak.cars.saloncars.service;

import mahdziak.cars.saloncars.Repository.UserRepository;
import mahdziak.cars.saloncars.dto.request.UserRequest;
import mahdziak.cars.saloncars.dto.request.UserSearchRequest;
import mahdziak.cars.saloncars.dto.response.DataResponse;
import mahdziak.cars.saloncars.dto.response.UserResponse;
import mahdziak.cars.saloncars.entity.User;
import mahdziak.cars.saloncars.specification.UserSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;


    public UserResponse save(UserRequest request){
        User user = new User();
        user.setLastName(request.getLastName());
        user.setFirstName(request.getFirstName());
        user.setBirthData(request.getBirthData());
        user.setCardNumber(request.getCardNumber());
        User saved = userRepository.save(user);
        return new UserResponse(saved);
    }

    public DataResponse<UserResponse> findAllByFilter(UserSearchRequest request) {
        Page<User> page = userRepository.findAll(
                new UserSpecification(request),
                request.getPaginationRequest().mapToPageRequest()
        );
        return new DataResponse<>(page.get().map(UserResponse::new).collect(Collectors.toList()),
                page.getTotalPages(), page.getTotalElements());
    }





}


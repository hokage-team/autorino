package mahdziak.cars.saloncars.specification;

import mahdziak.cars.saloncars.dto.request.CarFilterRequest;
import mahdziak.cars.saloncars.entity.Car;
import mahdziak.cars.saloncars.entity.Country;
import mahdziak.cars.saloncars.entity.Model;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class CarSpecification implements Specification<Car> {
    private CarFilterRequest filter;

    public CarSpecification(CarFilterRequest filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<Car> r, CriteriaQuery<?> cq, CriteriaBuilder cb) {
        List<Predicate> predicates = new ArrayList<>();

        Predicate byYear = findByYear(r, cb);
        if (byYear != null) predicates.add(byYear);


        Predicate byVolume = findByVolume(r, cb);
        if (byVolume != null) predicates.add(byVolume);

        Predicate byNameLike = findByNameLike(r, cb);
        if (byNameLike != null) predicates.add(byNameLike);

        Predicate byCountry = findByCountry(r, cb);
        if (byCountry != null) predicates.add(byCountry);


        Predicate byModel = findByModel(r, cb);
        if (byModel != null) predicates.add(byModel);

        return cb.and(predicates.toArray(new Predicate[0]));
    }

    private Predicate findByCountry(Root<Car> r, CriteriaBuilder cb) {
        Join<Car, Country> countryJoin = r.join("country");
        return countryJoin.get("id").in(filter.getCountriesId().toArray());
    }


    private Predicate findByModel(Root<Car> r, CriteriaBuilder cb) {
        Join<Car, Model> modelJoin = r.join("model");
        return modelJoin.get("id").in(filter.getModelsId().toArray());
    }

    private Predicate findByNameLike(Root<Car> r, CriteriaBuilder cb) {
        String name = filter.getName();
        if (name == null || name.trim().isEmpty()) {
            return null;
        }
        return cb.like(r.get("name"), '%' + name + '%');
    }

    private Predicate findByYear(Root<Car> r, CriteriaBuilder cb) {
        if (filter.getYearFrom() == null && filter.getYearTo() == null) {
            return null;
        }
        if (filter.getYearFrom() == null) {
            filter.setYearFrom(0);
        }
        if (filter.getYearTo() == null) {
            filter.setYearTo(Integer.MAX_VALUE);
        }
        return cb.between(r.get("year"), filter.getYearFrom(), filter.getYearTo());
    }

    private Predicate findByVolume(Root<Car> r, CriteriaBuilder cb) {
        if (filter.getVolumeFrom() == null && filter.getVolumeTo() == null) {
            return null;
        }
        if (filter.getVolumeFrom() == null) {
            filter.setVolumeFrom(0d);
        }
        if (filter.getVolumeTo() == null) {
            filter.setVolumeTo(Double.MAX_VALUE);
        }
        return cb.between(r.get("volume"), filter.getVolumeFrom(), filter.getVolumeTo());
    }
}



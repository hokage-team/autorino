package mahdziak.cars.saloncars.specification;

import mahdziak.cars.saloncars.dto.request.UserSearchRequest;
import mahdziak.cars.saloncars.entity.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class UserSpecification implements Specification<User> {


    private UserSearchRequest filter;

    public UserSpecification(UserSearchRequest filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<User> r, CriteriaQuery<?> cq, CriteriaBuilder cb) {
        if (filter.getLastName() == null && filter.getFirstName() == null && filter.getBirthData() == null && filter.getEmail() == null) {
            return null;
        }

        List<Predicate> predicates = new ArrayList<>();
        if (filter.getLastName() != null && !filter.getLastName().trim().isEmpty()) {
            predicates.add(cb.like(r.get("lastName"), filter.getLastName()));
        }
        if (filter.getFirstName() != null && !filter.getFirstName().trim().isEmpty()) {
            predicates.add(cb.like(r.get("firstName"), filter.getFirstName()));
        }

        if (filter.getBirthData() != null && !filter.getBirthData().trim().isEmpty()) {
            predicates.add(cb.like(r.get("birthData"), filter.getBirthData()));
        }
        if (filter.getEmail() != null && !filter.getEmail().trim().isEmpty()) {
            predicates.add(cb.like(r.get("email"), filter.getEmail()));
        }

        return cb.or(predicates.toArray(new Predicate[0]));

    }
}
